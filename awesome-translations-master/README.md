# awesome-translations

[![Awesome](https://awesome.re/badge-flat.svg)](https://awesome.re)

😎 Awesome lists about Internationalization & localization stuff. Translations! t9n, l10n, g11n, m17n, i18n.

![earth-night](https://github.com/mbiesiad/awesome-translations/blob/master/media/earth-night.png)

> _Source: Pexels.com, image by Pixabay: https://www.pexels.com/photo/black-textile-41949/_

# Content

* [Awesome platforms](#awesome-platforms)
* [Other](#other)
* [Articles](#articles)
* [Contributing](#contributing)
* [Code of Conduct](#code-of-conduct)
* [License](#license)

# Awesome platforms

**i18n**

* [Crowdin](https://crowdin.com/) - is a closed source cloud-based localization technology and services company. Offered as a software as a service for commercial products and free of charge for non-commercial open source projects and educational projects
* [GitLocalize](https://gitlocalize.com/) - is a powerful localization platform that syncs with your GitHub repository and lets you continuously translate its content.
* [LocaleApp](https://www.localeapp.com/) - Locale: Effective App Localization and Translation
* [Transifex](https://www.transifex.com/) - is a proprietary, web-based translation platform, globalization management system (GMS)
* [Weblate](https://weblate.org/) - is a platform for one of the most positive and empowering communities of libre software
* [Zanata](http://zanata.org/) - is a web-based translation platform for translators, content creators and developers to manage localisation projects

# Other

* [Angular and i18n](https://angular.io/guide/i18n) - Angular i18n guide
* [Electronjs i18n](https://www.electronjs.org/apps/i18n-manager) - i18n Manager
* [Flutter i18n](https://flutter.dev/docs/development/accessibility-and-localization/internationalization) - Internationalizing Flutter apps
* [I18next](https://www.i18next.com/) - is an internationalization-framework written in and for JavaScript
* [Google Crowdsource](https://crowdsource.google.com/) - is a crowdsourcing platform developed by Google intended to improve a host of Google services through the user-facing training of different algorithms (with Translation and translation validation section)
* [Localizejs](https://localizejs.com/)
* [MDN i18n](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/i18n) - APIs
* [npm i18n](https://www.npmjs.com/package/i18n) - Lightweight simple translation module with dynamic json storage
* [react-i18next](https://react.i18next.com/) - is a powerful internationalization framework for React / React Native which is based on i18next
* [Ruby I18n](https://guides.rubyonrails.org/i18n.html) - Rails Internationalization (I18n) API
* [Quasar I18n](https://quasar.dev/options/app-internationalization) - App Internationalization (I18n)
* [Vue I18n](https://kazupon.github.io/vue-i18n/) - is internationalization plugin for Vue.js
* [LocalizationLab](https://www.localizationlab.org/) - global community of 7000+ contributors who support the translation and localization of Internet freedom tools - technologies that address access, security, digital literacy, and anonymity online to ensure that people around the world have safe avenues for accessing information on the Internet.

# Articles

* [Debian manuals](https://www.debian.org/doc/manuals/debian-reference/ch08.en.html) - Chapter 8: I18N and L10N
* [Developer Chrome i18n](https://developer.chrome.com/webstore/i18n) - Internationalizing Your App
* [Software Globalization, Berkeley.edu - slides](https://lx.berkeley.edu/sites/default/files/berkeleylinguisticsdeptg11ncldr.pdf) - "Software Globalization and Adding Languages on Computers and Mobile Devices" by Craig Cummings (slides)
* [Stack Overflow Q&A-1](https://stackoverflow.com/questions/506743/localization-and-internationalization-whats-the-difference) - Localization and internationalization, what's the difference?
* [Stack Overflow Q&A-2](https://stackoverflow.com/questions/754520/what-is-the-actual-differences-between-i18n-l10n-g11n-and-specifically-what-does) - What is the actual differences between I18n/L10n/G11n and specifically what does each mean for development?
* [W3C](https://www.w3.org/International/questions/qa-i18n) - Localization vs. Internationalization

![earth](https://github.com/mbiesiad/awesome-translations/blob/master/media/earth.png)

> _Source: Pexels.com, image by Pixabay: https://www.pexels.com/photo/earth-space-universe-globe-41953/_

# Contributing

Warmly welcome! Kindly go through [Contribution Guidelines](CONTRIBUTING.md) first.

# Code of Conduct

Examples of behavior that contributes to creating a positive environment include:

    Using welcoming and inclusive language
    Being respectful of differing viewpoints and experiences
    Gracefully accepting constructive criticism
    Focusing on what is best for the community
    Showing empathy towards other community members

# License
Free [MIT](LICENSE) license.

See also other [AWESOME here](https://github.com/mbiesiad/awesome-chess) and [here](https://github.com/mbiesiad/awesome-astronomy). :fire:

__________________________________________________

Created by @[mbiesiad](https://github.com/mbiesiad)
